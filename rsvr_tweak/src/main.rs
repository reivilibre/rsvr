use structopt::StructOpt;

use std::path::PathBuf;
use rsvr_profile::{KitProfile, AudioOutputParams, HeadMountParams, VideoOutputParams, VideoDisplayParams};
use std::fs::File;
use std::io::Write;

#[derive(StructOpt, Debug)]
enum Commands {
	#[structopt(name = "generate", alias = "gen")]
	Generate {
		#[structopt(parse(from_os_str))]
		path: PathBuf
	},

	#[structopt(name = "interactive")]
	Interactive {
		#[structopt(parse(from_os_str))]
		profile_path: PathBuf,

		#[structopt(parse(from_os_str))]
		object_path: PathBuf,
	},
}

mod interactive;

fn main() {
	let args = Commands::from_args();
	println!("{:#?}", args);
	match args {
		Commands::Generate { path } => {
			let new_kit = KitProfile::WebRtc {
				video: VideoOutputParams {
					display: VideoDisplayParams {
						width_px: 1920,
						height_px: 1080,
						width_m: 0.116,
						height_m: 0.065,
						bottom_bezel_offset_m: 0.003,
					},
					render_texture_size: [1024, 1024],
					head_mount: HeadMountParams {
						screen_lens_distance_m: 0.036,
						inter_lens_distance_m: 0.055,
						fov_angle_degrees: 50.0,
						tray_lens_distance_m: 0.035,
						pincushion_distortion_coefficients: vec![0.1, 1.0],
					},
				},
				audio: AudioOutputParams {},
				input: Vec::new(),
				signalling_socket: "127.0.0.1:8087".parse().unwrap(),
				connection_id: String::from("default"),
				video_encoder: rsvr_profile::VideoEncoder::Vp8Enc,
				framerate: (50, 1),
			};

			let mut output_file = File::create(&path).expect("Unable to open file for writing");
			let serialised = ron::ser::to_string_pretty(&new_kit, Default::default()).unwrap();
			output_file.write(serialised.as_bytes()).expect("Unable to write to file");
		}
		Commands::Interactive { profile_path, object_path } => {
			let input_file = File::open(profile_path).expect("Unable to open profile file");
			let mut profile: KitProfile = ron::de::from_reader(input_file).expect("Failed to load profile");

			let orig_profile = profile.clone();

			self::interactive::main(&object_path, &mut profile);

			// TODO export view
		}
	}
}
