use nalgebra_glm::{Mat4, Vec3};

/// Helper that helps type checking
pub fn array_m4(m: Mat4) -> [[f32; 4]; 4] {
	m.into()
}

pub fn array_v3(v: Vec3) -> [f32; 3] { v.into() }

