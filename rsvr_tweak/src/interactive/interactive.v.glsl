#version 140

in vec3 position;
in vec2 texture_coord;
in vec3 normal;

out vec3 vf_world_position;
out vec2 vf_texture_coord;
out vec3 vf_normal;

uniform mat4 model;
uniform mat4 view_projection;

void main() {
	vec4 world_position = model * vec4(position, 1.0);
	vf_world_position = world_position.xyz;
	gl_Position = view_projection * world_position;
	vf_texture_coord = texture_coord;
	vf_normal = normal;
}
