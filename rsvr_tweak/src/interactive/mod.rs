/*
 * Copyright 2018, olivier@librepush.net
 *
 * This file is part of RSVR.
 */
use nalgebra_glm as glm;

use std::path::Path;

use glium::{
	implement_vertex, uniform, DrawParameters, Depth, DepthTest, Display, VertexBuffer, IndexBuffer,
	texture::{SrgbTexture2d, RawImage2d},
};
use glium::index::PrimitiveType;

use glium::glutin::{
	self,
	dpi::*,
	VirtualKeyCode,
	ElementState,
};

use std::fs::File;

use std::time::Instant;

use glium::Surface;

mod util;

use std::f32;

use std::collections::HashSet;

use std::io::BufReader;

use self::util::{array_m4, array_v3};

use wavefront_obj::{mtl::{MtlSet, Material}};

mod model;

use std::collections::HashMap;

use std::rc::Rc;

use self::model::PerMaterialGeometry;

use rsvr_profile::{KitProfile, VideoOutputParams};

use rsvr_opengl::StereoscopicFrameBuffer;

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Vertex {
	position: [f32; 3],
	texture_coord: [f32; 2],
	normal: [f32; 3],
}

implement_vertex!(Vertex, position, texture_coord, normal);

//pub type BufferPair = (VertexBuffer<Vertex>, IndexBuffer<u16>);

pub struct RenderGroup {
	vertices: VertexBuffer<Vertex>,
	indices: IndexBuffer<u16>,
	texture: Rc<SrgbTexture2d>,
	ka: [f32; 3],
	kd: [f32; 3],
	ks: [f32; 3],
	phong_shininess: f32,
}

pub fn load_textures(obj_path: &Path, mtlset: &MtlSet, display: &Display) -> Result<HashMap<String, Rc<SrgbTexture2d>>, Box<std::error::Error>> {
	let mut result = HashMap::new();
	for material in mtlset.materials.iter() {
		// uv_map is a misleading name; this is actually the Kd (diffuse) map
		if let Some(ref kd_map_name) = material.uv_map {
			let image_file_path = obj_path.with_file_name(kd_map_name);
			let image_file = File::open(&image_file_path)
				.map_err(|e| format!("Failed to open {:?}: {:?}", image_file_path, e))?;
			let image = image::load(BufReader::new(image_file),
			                        image::JPEG).unwrap().to_rgba();
			let image_dimensions = image.dimensions();
			let image = RawImage2d::from_raw_rgba_reversed(&image.into_raw(), image_dimensions);
			let gpu_texture = SrgbTexture2d::new(display, image).unwrap();

			result.insert(material.name.clone(), Rc::new(gpu_texture));
		}
	}
	Ok(result)
}

pub fn gen_buffers(object: &HashMap<String, PerMaterialGeometry>, mtlset: &MtlSet, textures: &HashMap<String, Rc<SrgbTexture2d>>, display: &Display) -> Vec<RenderGroup> {
	let material_map: HashMap<String, &Material> = mtlset.materials.iter().map(|material| {
		(material.name.clone(), material)
	}).collect();

	let mut result = Vec::new();

	for (objmat_name, objmat_geom) in object.iter() {
		let material = *material_map.get(objmat_name).expect("Undefined material");
		let vertex_buffer = glium::VertexBuffer::new(display, &objmat_geom.vertices).unwrap();
		let index_buffer = glium::IndexBuffer::new(display, PrimitiveType::TrianglesList, &objmat_geom.indices).unwrap();

		result.push(RenderGroup {
			vertices: vertex_buffer,
			indices: index_buffer,
			texture: textures.get(objmat_name).expect("No diffuse map texture found").clone(),
			ka: model::colour_to_array(&material.color_ambient),
			kd: model::colour_to_array(&material.color_diffuse),
			ks: model::colour_to_array(&material.color_specular),
			phong_shininess: material.specular_coefficient as f32,
		});
	}

	result
}

pub fn main(object_path: &Path, kit_profile: &mut KitProfile) {
	// First load the object
	let (objset, mtlset_opt) = model::load_model(object_path).expect("Failed to load model");
	if objset.objects.len() != 1 {
		eprintln!("warning: ObjSet length isn't one, it's {}", objset.objects.len());
	}

	let object = objset.objects.get(0).unwrap();

	let verts = model::object_to_vertices(&object).expect("Failed to load geometry");

	let mut events_loop = glutin::EventsLoop::new();
	let window = glutin::WindowBuilder::new()
		.with_title("rsvr_tweak")
		.with_dimensions(LogicalSize::new(1024.0, 512.0));

	let context = glutin::ContextBuilder::new()
		.with_vsync(true);

	let display = glium::Display::new(window, context, &events_loop).unwrap();

	// Now load the textures onto the GPU
	let mtlset = mtlset_opt.expect("I only accept objects with materials");

	let texture_dict = load_textures(object_path, &mtlset, &display)
		.expect("Failed to load textures");

	// Then load the geometry onto the GPU
	let render_groups = gen_buffers(&verts, &mtlset, &texture_dict, &display);

	eprintln!("Going to construct display.");
	let mut final_display = rsvr_opengl::construct_display(&display, kit_profile)
		.expect("Unable to construct final display");
	eprintln!("Display constructed.");


	let vr_video = kit_profile.video();
	let eye_width = vr_video.render_texture_size[0];
	let eye_height = vr_video.render_texture_size[1];

	// Create the eye framebuffers
	let mut stereo_framebuffer = StereoscopicFrameBuffer::new(&display, eye_width, eye_height)
		.expect("Unable to create Stereoscopic Framebuffers");

	//let mut final_display = rsvr_opengl::OnScreenDisplay::new(&display);

	let mut display_state = rsvr_opengl::DisplayState::new(vr_video.clone(), &display)
		.expect("Unable to create target rendering state");

	// camera space → screen space
	let projection = glm::perspective(
		(eye_width as f32) / (eye_height as f32), 70.0, 0.5, 100.0,
	);

	println!("proj {:?}", projection);


	let vertex_shader_src = include_str!("interactive.v.glsl");
	let fragment_shader_src = include_str!("interactive.f.glsl");

	let program = glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap();

	let mut pushed_keys = HashSet::new();

	let model_pos = glm::vec3(2.0, 0.5, -2.0);
	let mut camera_pos = glm::vec3(0.0, 1.0, 0.0);

	// get a unit quaternion with no rotation
	let mut camera_direction = glm::quat(0.0, 0.0, 0.0, 1.0);

	let half_eye_sep = vr_video.head_mount.inter_lens_distance_m / 2.0;

	let mut last_fps_report = Instant::now();
	let mut frames_drawn = 0;

	let mut running = true;
	while running {
		let model = glm::translation(&model_pos);

		// we first find out the direction of the camera's x (i.e. right).
		let camera_right = glm::normalize(&glm::quat_cross_vec(&camera_direction, &glm::vec3(1.0, 0.0, 0.0)));

		let camera_pos_left = camera_pos - half_eye_sep * camera_right;
		let camera_pos_right = camera_pos + half_eye_sep * camera_right;

		let view_left = glm::translation(&(-camera_pos_left));
		let view_left = glm::quat_to_mat4(&(-camera_direction)) * view_left;
		let view_projection_left = projection * view_left;

		let view_right = glm::translation(&(-camera_pos_right));
		let view_right = glm::quat_to_mat4(&(-camera_direction)) * view_right;
		let view_projection_right = projection * view_right;

		events_loop.poll_events(|event| {
			match event {
				glutin::Event::WindowEvent { event, .. } => match event {
					glutin::WindowEvent::CloseRequested => {
						running = false
					}
					glutin::WindowEvent::Resized(_logical_size) => {
						//let dpi_factor = gl_window.get_hidpi_factor();
						//display.resize(logical_size.to_physical(dpi_factor));
					}
					glutin::WindowEvent::KeyboardInput { input, .. } => {
						if let Some(virtual_keycode) = input.virtual_keycode {
							match input.state {
								ElementState::Pressed => {
									pushed_keys.insert(virtual_keycode);
								}
								ElementState::Released => {
									pushed_keys.remove(&virtual_keycode);
								}
							}
						}

						/*if input.state == ElementState::Released {
							println!("{:?}", input);
						}*/
					}
					_ => ()
				},
				_ => ()
			}
		});

		if pushed_keys.contains(&VirtualKeyCode::Numpad8) {
			let q = glm::quat_normalize(&glm::quat(-1.0, 0.0, 0.0, 60.0));
			//let qp = glm::quat_conjugate(&q);
			// q * P * q' is only if P is a point with Re(P) = 0 and Im(P) = xi + yj + zk.
			//camera_direction = glm::quat_cross(&glm::quat_cross(&q, &camera_direction), &qp);

			camera_direction = q * camera_direction;
		}
		if pushed_keys.contains(&VirtualKeyCode::Numpad2) {
			camera_direction = glm::quat_normalize(&glm::quat(1.0, 0.0, 0.0, 60.0)) * camera_direction;
		}

		if pushed_keys.contains(&VirtualKeyCode::Numpad4) {
			camera_direction = glm::quat_normalize(&glm::quat(0.0, -1.0, 0.0, 60.0)) * camera_direction;
		}
		if pushed_keys.contains(&VirtualKeyCode::Numpad6) {
			camera_direction = glm::quat_normalize(&glm::quat(0.0, 1.0, 0.0, 60.0)) * camera_direction;
		}

		if pushed_keys.contains(&VirtualKeyCode::Q) {
			camera_direction = glm::quat_normalize(&glm::quat(0.0, 0.0, -1.0, 60.0)) * camera_direction;
		}
		if pushed_keys.contains(&VirtualKeyCode::E) {
			camera_direction = glm::quat_normalize(&glm::quat(0.0, 0.0, 1.0, 60.0)) * camera_direction;
		}


		if pushed_keys.contains(&VirtualKeyCode::W) {
			//camera_pos += 0.05 * glm::quat_rotate_vec3(&camera_direction, &glm::vec3(0.0, 0.0, 1.0));
			camera_pos += 0.05 * &glm::quat_inv_cross_vec(&glm::vec3(0.0, 0.0, -1.0), &camera_direction);
		}
		if pushed_keys.contains(&VirtualKeyCode::S) {
			camera_pos += 0.05 * &glm::quat_inv_cross_vec(&glm::vec3(0.0, 0.0, 1.0), &camera_direction);
		}

		if pushed_keys.contains(&VirtualKeyCode::A) {
			camera_pos += 0.05 * &glm::quat_inv_cross_vec(&glm::vec3(-1.0, 0.0, 0.0), &camera_direction);
		}
		if pushed_keys.contains(&VirtualKeyCode::D) {
			camera_pos += 0.05 * &glm::quat_inv_cross_vec(&glm::vec3(1.0, 0.0, 0.0), &camera_direction);
		}

		// // // RSVR Tweak Keybinds... for Tweaking VR Profiles

		// Inter-lens (inter-eye) Distance -= (= is plus without shift)
		if pushed_keys.contains(&VirtualKeyCode::Equals) {
			kit_profile.video_mut().head_mount.inter_lens_distance_m += 0.0001;
			display_state.replace_vr_profile(kit_profile.video(), &display).unwrap();
		}
		if pushed_keys.contains(&VirtualKeyCode::Subtract) {
			kit_profile.video_mut().head_mount.inter_lens_distance_m -= 0.0001;
			display_state.replace_vr_profile(kit_profile.video(), &display).unwrap();
		}

		// Screen to lens distance
		if pushed_keys.contains(&VirtualKeyCode::RBracket) {
			kit_profile.video_mut().head_mount.screen_lens_distance_m += 0.0001;
			display_state.replace_vr_profile(kit_profile.video(), &display).unwrap();
		}

		if pushed_keys.contains(&VirtualKeyCode::LBracket) {
			kit_profile.video_mut().head_mount.screen_lens_distance_m -= 0.0001;
			display_state.replace_vr_profile(kit_profile.video(), &display).unwrap();
		}

		// Tray to Lens Distance. Be warned that this has a degree of freedom with Bottom Bezel!
		if pushed_keys.contains(&VirtualKeyCode::I) {
			kit_profile.video_mut().head_mount.tray_lens_distance_m += 0.0001;
			display_state.replace_vr_profile(kit_profile.video(), &display).unwrap();
		}

		if pushed_keys.contains(&VirtualKeyCode::K) {
			kit_profile.video_mut().head_mount.tray_lens_distance_m -= 0.0001;
			display_state.replace_vr_profile(kit_profile.video(), &display).unwrap();
		}


		// // // End of RSVR Tweak Keybinds


		let (target_left, target_right) = unsafe {
			stereo_framebuffer.glium_surfaces()
		};

		target_left.clear_color(0.5, 0.8, 0.7, 1.0);
		target_right.clear_color(0.5, 0.8, 0.7, 1.0);
		target_left.clear_depth(f32::INFINITY);
		target_right.clear_depth(f32::INFINITY);

		let dparams = &DrawParameters {
			depth: Depth {
				test: DepthTest::IfLess,
				write: true,
				..Default::default()
			},
			..Default::default()
		};

		for render_group in render_groups.iter() {
			target_left.draw(
				&render_group.vertices, &render_group.indices,
				&program,
				&uniform! {
					// Vertex Shader
					model: array_m4(model),
					view_projection: array_m4(view_projection_left),

					// Fragment Shader
					intensity_ambient: [0.05f32, 0.04f32, 0.03f32],
					intensity_diffuse: [0.85f32, 0.82f32, 0.75f32],
					intensity_specular: [0.85f32, 0.85f32, 0.85f32],
					intensity_diffuse2: [0.95f32, 0.1f32, 0.05f32],
					intensity_specular2: [1.0f32, 0.0f32, 0.0f32],
					light_position2: [2.0f32, 1.5f32, -2.0f32],
					camera_position: array_v3(camera_pos_left.clone()),

					tex_diffuse: &*render_group.texture,

					// Fragment Shader (material)
					mtl_ka: render_group.ka,
					mtl_kd: render_group.kd,
					mtl_ks: render_group.ks,
					mtl_phong_shininess: render_group.phong_shininess,
			}, dparams).unwrap();
			target_right.draw(
				&render_group.vertices, &render_group.indices,
				&program,
				&uniform! {
					// Vertex Shader
					model: array_m4(model),
					view_projection: array_m4(view_projection_right),

					// Fragment Shader
					intensity_ambient: [0.05f32, 0.04f32, 0.03f32],
					intensity_diffuse: [0.85f32, 0.82f32, 0.75f32],
					intensity_specular: [0.85f32, 0.85f32, 0.85f32],
					intensity_diffuse2: [0.95f32, 0.1f32, 0.05f32],
					intensity_specular2: [1.0f32, 0.0f32, 0.0f32],
					light_position2: [2.0f32, 1.5f32, -2.0f32],
					camera_position: array_v3(camera_pos_right.clone()),

					tex_diffuse: &*render_group.texture,

					// Fragment Shader (material)
					mtl_ka: render_group.ka,
					mtl_kd: render_group.kd,
					mtl_ks: render_group.ks,
					mtl_phong_shininess: render_group.phong_shininess,
			}, dparams).unwrap();
		}

		/*target.draw(&vertex_buffer, &index_buffer, &program,
		            &uniform! { perspective: array_m4(view_projection) },
		            dparams).unwrap();*/


		//target.finish().unwrap();

		final_display.finalise_frame(&mut stereo_framebuffer, &mut display_state).unwrap();

		frames_drawn += 1;
		let elapsed_since_last_report = last_fps_report.elapsed();
		if elapsed_since_last_report.as_secs() > 10 {
			let as_float = elapsed_since_last_report.as_secs() as f32 + (elapsed_since_last_report.subsec_nanos() as f32 * 1e-9);
			let frame_rate = frames_drawn as f32 / as_float;
			eprintln!("Frame rate: {} fps", frame_rate);
			eprintln!("Keys: {:#?}", pushed_keys);

			frames_drawn = 0;
			last_fps_report = Instant::now();
		}
	}
}
