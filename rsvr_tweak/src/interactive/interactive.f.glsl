#version 140

in vec3 vf_world_position;
in vec2 vf_texture_coord;
in vec3 vf_normal;

// Lighting Parameters
uniform vec3 intensity_ambient, intensity_diffuse, intensity_specular, intensity_diffuse2, intensity_specular2;
uniform vec3 light_position2;

// Material Parameters
uniform vec3 mtl_ka, mtl_kd, mtl_ks;
uniform float mtl_phong_shininess;
uniform sampler2D tex_diffuse;

// Viewer Parameters
uniform vec3 camera_position;

out vec3 fragment_colour;

/// Basic Phong Illumination
void main() {
	// L
	const vec3 light_from = normalize(vec3(2, 1.5, -0.5));
	vec3 light_from2 = normalize(light_position2 - vf_world_position);
	
	// V
	vec3 to_camera_norm = normalize(camera_position - vf_world_position);
	
	vec3 normal = normalize(vf_normal);
	
	vec3 perfect_reflection = 2 * dot(light_from, normal) * normal - light_from;
	vec3 perfect_reflection2 = 2 * dot(light_from2, normal) * normal - light_from2;
	
	vec3 sample_diffuse = texture2D(tex_diffuse, vf_texture_coord).rgb;
	
	vec3 ambient = mtl_ka * intensity_ambient;
	vec3 diffuse = sample_diffuse * mtl_kd * max(0, dot(light_from, normal)) * intensity_diffuse;
	vec3 specular = /* sample_specular * */ mtl_ks * pow(max(0, dot(perfect_reflection, to_camera_norm)), mtl_phong_shininess) *
	intensity_specular;
	vec3 diffuse2 = sample_diffuse * mtl_kd * max(0, dot(light_from2, normal)) * intensity_diffuse2;
	vec3 specular2 = /* sample_specular * */ mtl_ks * pow(max(0, dot(perfect_reflection2, to_camera_norm)), mtl_phong_shininess) *
	intensity_specular2;

	fragment_colour = ambient + diffuse + specular + diffuse2 + specular2;
}
