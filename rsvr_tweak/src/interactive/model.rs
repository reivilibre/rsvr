use std::path::Path;
use std::fs::File;
use std::error::Error;
use wavefront_obj::{obj::{self, ObjSet, Object, Primitive, VTNIndex, Vertex, TVertex}, mtl::{self, MtlSet, Color as Colour}};
use std::io::Read;
use std::collections::HashMap;

use super::Vertex as GPUVertex;

pub fn load_model(obj_path: &Path) -> Result<(ObjSet, Option<MtlSet>), Box<Error>> {
	let mut object_file = File::open(obj_path)
		.or_else(|err| Err(format!("When loading obj file: {:#?}", err)))?;
	let mut object_text = String::new();
	object_file.read_to_string(&mut object_text)?;

	let object_set = obj::parse(object_text)
		.or_else(|err| Err(format!("When parsing obj file: {:#?}", err)))?;


	let material_set = match object_set.material_library {
		None => None,
		Some(ref material_filename) => {
			let material_path = obj_path.with_file_name(material_filename);
			let mut material_file = File::open(material_path)
				.or_else(|err| Err(format!("When loading mtl file: {:#?}", err)))?;
			let mut material_text = String::new();
			material_file.read_to_string(&mut material_text)?;

			let material_set = mtl::parse(material_text)
				.or_else(|err| Err(format!("When parsing mtl file: {:#?}", err)))?;

			Some(material_set)
		}
	};

	Ok((object_set, material_set))
}

#[derive(Default, Debug, Clone)]
pub struct PerMaterialGeometry {
	pub vertices: Vec<GPUVertex>,
	pub indices: Vec<u16>,
}

fn vert_to_tuple(vert: &Vertex) -> (f64, f64, f64) {
	(vert.x, vert.y, vert.z)
}

fn tvert2d_to_tuple(tvert: &TVertex) -> (f64, f64) {
	(tvert.u, tvert.v)
}

pub fn colour_to_array(colour: &Colour) -> [f32; 3] {
	[colour.r as f32, colour.g as f32, colour.b as f32]
}

fn dedupe_vertex(object: &Object, (vertex_idx, texture_idx_opt, normal_idx_opt): VTNIndex, vertices: &mut Vec<GPUVertex>) -> Result<u16, Box<Error>> {
	let position_coord = vert_to_tuple(object.vertices.get(vertex_idx).ok_or("No positional vertex at that index")?);
	let texture_coord = match texture_idx_opt {
		None => {
			(0.0, 0.0)
		}
		Some(texture_idx) => {
			tvert2d_to_tuple(object.tex_vertices.get(texture_idx).ok_or("No texture vertex at that index")?)
		}
	};
	let normal_coord = match normal_idx_opt {
		None => {
			(0.0, 0.0, 0.0)
		}
		Some(normal_index) => {
			vert_to_tuple(object.normals.get(normal_index).ok_or("No normal vertex at that index")?)
		}
	};

	let vertex = GPUVertex {
		position: [position_coord.0 as f32, position_coord.1 as f32, position_coord.2 as f32],
		texture_coord: [texture_coord.0 as f32, texture_coord.1 as f32],
		normal: [normal_coord.0 as f32, normal_coord.1 as f32, normal_coord.2 as f32],
	};

	let already_at = vertices.iter().position(|&v| v == vertex);
	let index = match already_at {
		None => {
			let position = vertices.len();
			vertices.push(vertex);
			position
		}
		Some(position) => {
			position
		}
	};
	Ok(index as u16)
}

/// Beware! This is naïve — for demonstration purposes only.
pub fn object_to_vertices(object: &Object) -> Result<HashMap<String, PerMaterialGeometry>, Box<Error>> {
	let mut materials: HashMap<String, PerMaterialGeometry> = HashMap::new();

	for geom in object.geometry.iter() {
		let matname = geom.material_name.as_ref()
			.map(|s| s.clone())
			.unwrap_or_else(|| "nomaterial".to_owned());
		let matentry = materials.entry(matname);
		let matgeom = matentry.or_default();
		for shape in geom.shapes.iter() {
			if let Primitive::Triangle(v1, v2, v3) = shape.primitive {
				let i1 = dedupe_vertex(object, v1, &mut matgeom.vertices)?;
				let i2 = dedupe_vertex(object, v2, &mut matgeom.vertices)?;
				let i3 = dedupe_vertex(object, v3, &mut matgeom.vertices)?;

				matgeom.indices.push(i1);
				matgeom.indices.push(i2);
				matgeom.indices.push(i3);
			}
		}
	}

	Ok(materials)
}