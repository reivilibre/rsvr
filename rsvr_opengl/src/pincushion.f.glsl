#version 140

/// This is the position we should sample from the texture BEFORE DISTORTION.
in vec2 frag_texcoord;

/// "Tan-angle units can be computed as distance on the screen divided by distance from the virtual eye to the screen"
/// This will be pre-computed. Of course, this is a small angle approximation, but it should be good enough and worthwhile to do?
uniform vec2 tan_angle_unit_scale;

/// The pincushion distortion coefficients
uniform float[2] pincushion_coefficients;

/// This is the texture which contains the rendering of the scene for the current eye.
uniform sampler2D tex;

/// The colour to be displayed on screen
out vec3 fragment_colour;

void main() {
	// Get the displacement from the optical centre, in texture co-ordinates.
	vec2 coord_from_optical_centre = (frag_texcoord - vec2(0.5, 0.5));
	
	vec2 coord_in_tan_angle_units = coord_from_optical_centre * tan_angle_unit_scale;
	
	float r_squared = (coord_in_tan_angle_units.x * coord_in_tan_angle_units.x) + (coord_in_tan_angle_units.y * coord_in_tan_angle_units.y);
	
	vec2 pincushion_distorted = coord_from_optical_centre *
		(1 +
			pincushion_coefficients[0] * r_squared +
			pincushion_coefficients[1] * r_squared * r_squared);
	
	vec2 back_in_texture = pincushion_distorted + vec2(0.5, 0.5);
	
	fragment_colour = texture2D(tex, back_in_texture).rgb/* * vec3(pincushion_coefficients[0], pincushion_coefficients[1], 1.0)*/;
}
