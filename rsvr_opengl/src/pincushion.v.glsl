#version 140

in vec2 position;
in vec2 texture_coord;

out vec2 frag_texcoord;

void main() {
	frag_texcoord = texture_coord;
	gl_Position = vec4(position, 1.0, 1.0);
}
