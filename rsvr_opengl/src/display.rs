// TODO This file is a bit of an absolute mess, needs cleaning up.

use std::borrow::{Borrow, Cow};
use std::net::SocketAddr;
use std::ops::Add;
use std::ops::Range;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::time::{Duration, Instant};

use glium::{
	BlitTarget,
	Display,
	DrawError,
	DrawParameters,
	Frame,
	framebuffer::MultiOutputFrameBuffer, framebuffer::SimpleFrameBuffer,
	implement_vertex, index::BufferCreationError as IndexBufferCreationError,
	index::IndicesSource,
	index::PrimitiveType,
	IndexBuffer,
	Program,
	Rect,
	Surface, texture::Texture2d, uniform,
	uniforms::{MagnifySamplerFilter, Uniforms},
	vertex::BufferCreationError as VertexBufferCreationError, vertex::MultiVerticesSource,
	VertexBuffer,
};

use rsvr_profile::{HeadMountParams, VideoDisplayParams, VideoOutputParams};
use rsvr_profile::KitProfile;
use rsvr_webrtc::{RsvrWebRtcError, WebRtcState};

use super::errors::{OpenglUnderlyingFailure, RsvrOpenglError};
use super::StereoscopicFrameBuffer;

rental! {
	pub mod rentals {
		use super::*;

		#[rental]
		pub struct OffscreenDisplay {
			texture: Box<Texture2d>,
			framebuffer: SimpleFrameBuffer<'texture>,
		}
	}
}

/// Represents a display which is on-screen.
pub struct OnScreenDisplay {
	glium_display: Display,
}

impl OnScreenDisplay {
	pub fn new(facade: &Display) -> OnScreenDisplay {
		OnScreenDisplay {
			glium_display: facade.clone()
		}
	}
}

/// Represents a display which is casted over WebRTC to a device like a phone.
pub struct WebRtcDisplay {
	offscreen_display: rentals::OffscreenDisplay,
	underlying_state: rsvr_webrtc::WebRtcState,
	last_frametime: Instant,
	frame_interval: Duration,
}

static HAS_INITIALISED_GSTREAMER: AtomicBool = AtomicBool::new(false);

impl WebRtcDisplay {
	fn new(facade: &Display, signalling_server: SocketAddr, connection_id: String,
	       width: u32, height: u32, framerate: (i32, i32)) -> Result<Self, RsvrOpenglError> {
		use glium::texture::{UncompressedFloatFormat, MipmapsOption};

		use crate::errors::OpenglUnderlyingFailure as Underlying;

		let texture = Texture2d::empty_with_format(
			facade, UncompressedFloatFormat::U8U8U8, MipmapsOption::NoMipmap,
			width, height,
		).map_err(|e| RsvrOpenglError::OpenglFailure {
			context: "Creating off-screen display texture for WebRTC",
			underlying: Underlying::TextureCreationError(e),
		})?;

		let offscreen_display = rentals::OffscreenDisplay::try_new_or_drop(
			Box::new(texture),
			|colour| {
				SimpleFrameBuffer::new(facade, &*colour)
					.map_err(|e| RsvrOpenglError::OpenglFailure {
						context: "Creating off-screen framebuffer for WebRTC",
						underlying: Underlying::ValidationError(e),
					})
			})?;


		let should_initialise_gst = !HAS_INITIALISED_GSTREAMER.swap(true, Ordering::Relaxed);

		if should_initialise_gst {
			eprintln!("Will initialise GStreamer.");
		} else {
			eprintln!("Won't initialise GStreamer.");
		}

		let seconds = framerate.1 / framerate.0;
		let nanos = (1e9 * (framerate.1 % framerate.0) as f64) / (framerate.0 as f64);

		let frame_interval = Duration::new(seconds as u64, nanos as u32);

		Ok(WebRtcDisplay {
			offscreen_display,
			underlying_state: WebRtcState::new(
				should_initialise_gst, width, height, framerate, signalling_server.to_string(),
				connection_id,
			)?,
			last_frametime: Instant::now(),
			frame_interval,
		})
	}
}


pub trait TargetDisplay: DynamicFinalisable {
	type UnderlyingSurface: Surface;

	/// Renders a frame. You need to pass in a callback which should render the frame, the whole
	/// frame, but nothing more than one frame.
	fn with_surface<F>(&mut self, callback: F)
	                   -> Result<(), RsvrOpenglError>
		where F: FnOnce(&mut Self::UnderlyingSurface) -> Result<(), RsvrOpenglError>;
}

pub trait DynamicFinalisable {
	/// Asks this object to call finalise_frame on itself with the correct associated trait...
	/// Needed to use monomorphisation at this level.
	fn finalise_frame(&mut self, stereoscopic_buffer: &mut StereoscopicFrameBuffer,
	                  display_state: &mut DisplayState) -> Result<(), RsvrOpenglError>;
}

impl TargetDisplay for OnScreenDisplay {
	type UnderlyingSurface = Frame;

	fn with_surface<F>(&mut self, callback: F)
	                   -> Result<(), RsvrOpenglError>
		where F: FnOnce(&mut Frame) -> Result<(), RsvrOpenglError> {
		let mut frame = self.glium_display.draw();
		callback(&mut frame)?;
		frame.finish().map_err(
			|e| RsvrOpenglError::OpenglFailure {
				context: "swapping target display (on-screen)",
				underlying: OpenglUnderlyingFailure::SwapBuffersError(e),
			}
		)?;
		Ok(())
	}
}

impl DynamicFinalisable for OnScreenDisplay {
	fn finalise_frame(&mut self, stereoscopic_buffer: &mut StereoscopicFrameBuffer, display_state: &mut DisplayState) -> Result<(), RsvrOpenglError> {
		finalise_frame(stereoscopic_buffer, self, display_state)
	}
}

pub struct WrappedSimpleFrameBuffer {
	pub underlying: rentals::OffscreenDisplay
}


impl WrappedSimpleFrameBuffer {
	unsafe fn framebuffer_mut(&mut self) -> &mut SimpleFrameBuffer {
		self.underlying.all_mut_erased().framebuffer
	}

	unsafe fn framebuffer_ref(&self) -> &SimpleFrameBuffer {
		self.underlying.all_erased().framebuffer
	}
}

impl Surface for WrappedSimpleFrameBuffer {
	fn clear(&mut self, rect: Option<&Rect>, colour: Option<(f32, f32, f32, f32)>, colour_srgb: bool, depth: Option<f32>, stencil: Option<i32>) {
		unsafe { self.framebuffer_mut() }
			.clear(rect, colour, colour_srgb, depth, stencil)
	}

	fn get_dimensions(&self) -> (u32, u32) {
		unsafe { self.framebuffer_ref() }.get_dimensions()
	}

	fn get_depth_buffer_bits(&self) -> Option<u16> {
		unsafe { self.framebuffer_ref() }.get_depth_buffer_bits()
	}

	fn get_stencil_buffer_bits(&self) -> Option<u16> {
		unsafe { self.framebuffer_ref() }.get_stencil_buffer_bits()
	}

	fn draw<'a, 'b, V, I, U>(&mut self, v: V, i: I, program: &Program, uniforms: &U,
	                         draw_parameters: &DrawParameters) -> Result<(), DrawError> where
		V: MultiVerticesSource<'b>, I: Into<IndicesSource<'a>>,
		U: Uniforms {
		unsafe { self.framebuffer_mut() }.draw(
			v, i, program, uniforms, draw_parameters,
		)
	}

	fn blit_from_frame(&self, source_rect: &Rect, target_rect: &BlitTarget, filter: MagnifySamplerFilter) {
		unsafe { self.framebuffer_ref() }
			.blit_from_frame(source_rect, target_rect, filter)
	}

	fn blit_from_simple_framebuffer(&self, source: &SimpleFrameBuffer, source_rect: &Rect, target_rect: &BlitTarget, filter: MagnifySamplerFilter) {
		unsafe { self.framebuffer_ref() }
			.blit_from_simple_framebuffer(source, source_rect, target_rect, filter)
	}

	fn blit_from_multioutput_framebuffer(&self, source: &MultiOutputFrameBuffer, source_rect: &Rect, target_rect: &BlitTarget, filter: MagnifySamplerFilter) {
		unsafe { self.framebuffer_ref() }
			.blit_from_multioutput_framebuffer(source, source_rect, target_rect, filter)
	}

	fn blit_color<S>(&self, source_rect: &Rect, target: &S, target_rect: &BlitTarget, filter: MagnifySamplerFilter) where S: Surface {
		unsafe { self.framebuffer_ref() }
			.blit_color(source_rect, target, target_rect, filter)
	}
}

impl TargetDisplay for WebRtcDisplay {
	type UnderlyingSurface = WrappedSimpleFrameBuffer;

	fn with_surface<F>(&mut self, callback: F) -> Result<(), RsvrOpenglError> where F: FnOnce(&mut Self::UnderlyingSurface) -> Result<(), RsvrOpenglError> {
		let mut result = None;

		take_mut::take(
			&mut self.offscreen_display,
			|offscreen_display| {
				let mut wrapped = WrappedSimpleFrameBuffer {
					underlying: offscreen_display
				};
				result = Some(callback(&mut wrapped));
				wrapped.underlying
			},
		);

		result.expect("Impossible: Callback not called")?;

		let texture = unsafe { self.offscreen_display.all_erased() }.texture;

		let mut next_frametime = self.last_frametime.add(self.frame_interval);

		let result = self.underlying_state.submit(|buffer| {
			let start_time = Instant::now();
			let image_data: RGBImageData = unsafe { texture.unchecked_read() };
			eprintln!("time unchecked read {:#?}", start_time.elapsed());

			let start_time = Instant::now();
			detupleify(image_data.data.borrow(), buffer);
			eprintln!("time detupleify {:#?}", start_time.elapsed());

			// because this isn't a monitor, there is no 'Vertical Sync' but we do want to limit
			// the frame-rate so we don't overload the stream...

			let now = Instant::now();

			if next_frametime < now {
				next_frametime = now;
			} else {
				eprintln!("sleeping {:?}", next_frametime - now);
				::std::thread::sleep(next_frametime - now);
			}
		}).map_err(|e| e.into());

		self.last_frametime = next_frametime;

		result
	}
}

struct RGBImageData {
	//pub data: Cow<'data, [(u8, u8, u8)]>
	pub data: Vec<(u8, u8, u8)>
}


/// Does the required transformation. Sadly it does do a copy.
fn detupleify(tuple_slice: &[(u8, u8, u8)], target: &mut [u8]) {
	assert_eq!(target.len(), 3 * tuple_slice.len(), "Cannot detupleify; wrong lengths.");
	for (i, (r, g, b)) in tuple_slice.iter().enumerate() {
		let offset = 3 * i;
		target[offset] = *r;
		target[offset + 1] = *g;
		target[offset + 2] = *b;
	}
}

impl glium::texture::Texture2dDataSink<(u8, u8, u8)> for RGBImageData {
	fn from_raw(data: Cow<[(u8, u8, u8)]>, width: u32, height: u32) -> Self {
		RGBImageData {
			data: data.into_owned()
		}
	}
}

impl DynamicFinalisable for WebRtcDisplay {
	fn finalise_frame(&mut self, stereoscopic_buffer: &mut StereoscopicFrameBuffer, display_state: &mut DisplayState) -> Result<(), RsvrOpenglError> {
		finalise_frame(stereoscopic_buffer, self, display_state)
	}
}

#[derive(Copy, Clone, Debug)]
struct EyeVertex {
	pub position: [f32; 2],
	pub texture_coord: [f32; 2],
}

implement_vertex!(EyeVertex, position, texture_coord);

struct EyeGeometry {
	pub vertices: VertexBuffer<EyeVertex>,
	pub indices: IndexBuffer<u16>,
	pub tan_angle_unit_scale: [f32; 2],
}

pub struct DisplayState {
	vr_video_params: VideoOutputParams,
	geometry: EyeGeometry,
	program: Program,
	draw_parameters: DrawParameters<'static>,
}

impl DisplayState {
	pub fn new(vr_video: VideoOutputParams, facade: &Display) -> Result<Self, RsvrOpenglError> {
		let geometry = Self::derive_geometry(&vr_video, facade)?;

		let vertex_shader_src = include_str!("pincushion.v.glsl");
		let fragment_shader_src = include_str!("pincushion.f.glsl");

		let program = glium::Program::from_source(facade, vertex_shader_src, fragment_shader_src, None).unwrap();

		let dparams = DrawParameters::default();

		Ok(DisplayState {
			vr_video_params: vr_video,
			geometry,
			program,
			draw_parameters: dparams,
		})
	}

	fn derive_geometry(vr_video: &VideoOutputParams, facade: &Display) -> Result<EyeGeometry, RsvrOpenglError> {
		let VideoOutputParams {
			head_mount: HeadMountParams {
				screen_lens_distance_m, inter_lens_distance_m, fov_angle_degrees,
				tray_lens_distance_m, ..
			},
			display: VideoDisplayParams {
				width_px, height_px, width_m, height_m, bottom_bezel_offset_m
			},
			..
		} = vr_video;

		let edge_to_optical_centre_m = tray_lens_distance_m - bottom_bezel_offset_m;

		let max_radius_m = screen_lens_distance_m * fov_angle_degrees.to_radians().tan();
		let centre_x_optical_centre_offset_m = inter_lens_distance_m / 2.0;
		let actual_radius_m = max_radius_m.min(centre_x_optical_centre_offset_m);

		let m_to_x_space = 2.0 / width_m;
		let m_to_y_space = 2.0 / height_m;

		let optical_centre_y = (-1.0) + m_to_y_space * edge_to_optical_centre_m;

		// CS for clip space
		let centre_x_optical_centre_offset_cs = m_to_x_space * centre_x_optical_centre_offset_m;
		let actual_radius_x_cs = m_to_x_space * actual_radius_m;
		let actual_radius_y_cs = m_to_y_space * actual_radius_m;

		// 2 * actual_radius_m = 1.0 texcoord
		let m_to_texture_space = 0.5 / actual_radius_m;

		let tan_angle_unit_scale = [
			1.0 / (m_to_texture_space * screen_lens_distance_m),
			1.0 / (m_to_texture_space * screen_lens_distance_m)
		];


		let vertices = [
			EyeVertex {
				position: [-centre_x_optical_centre_offset_cs - actual_radius_x_cs, optical_centre_y + actual_radius_y_cs],
				texture_coord: [0.0, 1.0],
			},
			EyeVertex {
				position: [-centre_x_optical_centre_offset_cs - actual_radius_x_cs, optical_centre_y - actual_radius_y_cs],
				texture_coord: [0.0, 0.0],
			},
			EyeVertex {
				position: [actual_radius_x_cs - centre_x_optical_centre_offset_cs, optical_centre_y - actual_radius_y_cs],
				texture_coord: [1.0, 0.0],
			},
			EyeVertex {
				position: [actual_radius_x_cs - centre_x_optical_centre_offset_cs, optical_centre_y + actual_radius_y_cs],
				texture_coord: [1.0, 1.0],
			},
			EyeVertex {
				position: [centre_x_optical_centre_offset_cs - actual_radius_x_cs, optical_centre_y + actual_radius_y_cs],
				texture_coord: [0.0, 1.0],
			},
			EyeVertex {
				position: [centre_x_optical_centre_offset_cs - actual_radius_x_cs, optical_centre_y - actual_radius_y_cs],
				texture_coord: [0.0, 0.0],
			},
			EyeVertex {
				position: [actual_radius_x_cs + centre_x_optical_centre_offset_cs, optical_centre_y - actual_radius_y_cs],
				texture_coord: [1.0, 0.0],
			},
			EyeVertex {
				position: [actual_radius_x_cs + centre_x_optical_centre_offset_cs, optical_centre_y + actual_radius_y_cs],
				texture_coord: [1.0, 1.0],
			},
		];

		let indices = [
			0, 1, 2,
			0, 2, 3,
			4, 5, 6,
			4, 6, 7
		];

		let vertex_buffer = glium::VertexBuffer::new(facade, &vertices)
			.map_err(|e| RsvrOpenglError::OpenglFailure {
				context: "Creating vertex buffer for eye vertices",
				underlying: OpenglUnderlyingFailure::VertexBufferCreationError(e),
			})?;
		let index_buffer = glium::IndexBuffer::new(facade, PrimitiveType::TrianglesList, &indices)
			.map_err(|e| RsvrOpenglError::OpenglFailure {
				context: "Creating index buffer for eye vertices",
				underlying: OpenglUnderlyingFailure::IndexBufferCreationError(e),
			})?;

		eprintln!("taus {:?}", tan_angle_unit_scale);

		Ok(EyeGeometry {
			vertices: vertex_buffer,
			indices: index_buffer,
			tan_angle_unit_scale,
		})
	}

	/// If it's legal to do so, replaces the VR profile. Otherwise, returns a message describing
	/// why the replacement could not be made.
	pub fn replace_vr_profile(&mut self, vr_video: &VideoOutputParams, facade: &Display) -> Result<(), RsvrOpenglError> {
		if vr_video.render_texture_size == self.vr_video_params.render_texture_size {
			self.vr_video_params = vr_video.clone();
			self.geometry = Self::derive_geometry(vr_video, facade)?;
			Ok(())
		} else {
			Err(RsvrOpenglError::ContractFailure {
				context: "replace_vr_profile",
				message: "Cannot dynamically change the render texture size.",
			})
		}
	}
}

pub fn construct_display(facade: &Display, kit_profile: &KitProfile) -> Result<Box<DynamicFinalisable>, RsvrOpenglError> {
	Ok(match kit_profile {
		KitProfile::Local { .. } => {
			Box::new(OnScreenDisplay::new(facade))
		}
		KitProfile::WebRtc { signalling_socket, connection_id, video, framerate, .. } => {
			let display = &video.display;
			Box::new(WebRtcDisplay::new(facade, signalling_socket.clone(), connection_id.clone(),
			                            display.width_px, display.height_px, *framerate)?)
		}
	})
}

pub fn finalise_frame<T: TargetDisplay>(stereoscopic_buffer: &mut StereoscopicFrameBuffer,
                                        target_display: &mut T,
                                        display_state: &mut DisplayState) -> Result<(), RsvrOpenglError> {
	target_display.with_surface(|surface| {
		surface.clear_color(0.0, 0.0, 0.0, 0.0);

		let (left_tex, right_tex) = stereoscopic_buffer.glium_textures_ref();

		const LEFT_QUAD_INDEX_RANGE: Range<usize> = 0..6;
		const RIGHT_QUAD_INDEX_RANGE: Range<usize> = 6..12;

		let geometry = &display_state.geometry;

		let mut pincushion = [0.0, 0.0];
		let pincushion_coefficients = &display_state.vr_video_params.head_mount.pincushion_distortion_coefficients[0..2];
		pincushion.copy_from_slice(pincushion_coefficients);

		surface.draw(&geometry.vertices, &geometry.indices.slice(LEFT_QUAD_INDEX_RANGE).expect("Impossible: No left quad in Index Buffer"),
		             &display_state.program,
		             &uniform! {
						tex: left_tex,
						pincushion_coefficients: pincushion,
						tan_angle_unit_scale: geometry.tan_angle_unit_scale
		             }, &display_state.draw_parameters)
			.map_err(|e| RsvrOpenglError::OpenglFailure {
				context: "Drawing left eye",
				underlying: OpenglUnderlyingFailure::DrawError(e),
			})?;

		surface.draw(&geometry.vertices, &geometry.indices.slice(RIGHT_QUAD_INDEX_RANGE).expect("Impossible: No right quad in Index Buffer"),
		             &display_state.program,
		             &uniform! {
						tex: right_tex,
						pincushion_coefficients: pincushion,
						tan_angle_unit_scale: geometry.tan_angle_unit_scale
		             }, &display_state.draw_parameters)
			.map_err(|e| RsvrOpenglError::OpenglFailure {
				context: "Drawing right eye",
				underlying: OpenglUnderlyingFailure::DrawError(e),
			})?;

		Ok(())
	})
}
