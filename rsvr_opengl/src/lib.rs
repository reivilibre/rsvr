// this 2015 syntax still needed for the rental! macro
#[macro_use]
extern crate rental;

pub use glium;

pub(crate) mod framebuffers;
pub(crate) mod errors;
pub(crate) mod display;

pub use self::errors::RsvrOpenglError;

pub use self::framebuffers::StereoscopicFrameBuffer;

pub use self::display::{TargetDisplay, DisplayState, OnScreenDisplay, WebRtcDisplay, finalise_frame, construct_display};