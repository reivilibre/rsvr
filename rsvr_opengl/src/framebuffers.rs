use glium::{
	texture::{
		MipmapsOption,
		UncompressedFloatFormat,
		DepthFormat,
		Texture2d,
	},
	framebuffer::{
		SimpleFrameBuffer,
		DepthRenderBuffer,
	},
	backend::Facade,
	Surface,
};

use super::errors::{RsvrOpenglError as Error, OpenglUnderlyingFailure as Underlying};

rental! {
	pub mod rentals {
		use super::*;

		#[rental]
		pub struct EyeFrameBuffer {
			underlying: Box<(Texture2d, DepthRenderBuffer)>,
			frame_buffer: SimpleFrameBuffer<'underlying>
		}
	}
}

use self::rentals::EyeFrameBuffer;

/// Contains OpenGL framebuffers, and underlying textures and depth renderbuffers, that should be
/// used as a render target when attempting to render stereoscopically / in VR.
pub struct StereoscopicFrameBuffer {
	/// The framebuffer (and underlying buffers) used for the left eye.
	pub(crate) left_eye: EyeFrameBuffer,
	/// The framebuffer (and underlying buffers) used for the right eye.
	pub(crate) right_eye: EyeFrameBuffer,
}

impl StereoscopicFrameBuffer {
	/// Creates a pair of framebuffers (with associated texture and depth buffers) that can be used
	/// as render targets for the two eyes.
	pub fn new<F: ?Sized>(facade: &F, eye_width: u32, eye_height: u32) -> Result<Self, Error>
		where F: Facade {
		// TODO allow more choice in the formats used for the library user

		pub fn new_eye<F: ?Sized>(context: &'static str, facade: &F, eye_width: u32, eye_height: u32) -> Result<EyeFrameBuffer, Error>
			where F: Facade {
			let eye_texture = Texture2d::empty_with_format(
				facade, UncompressedFloatFormat::U16U16U16, MipmapsOption::NoMipmap,
				eye_width, eye_height,
			).map_err(|e| Error::OpenglFailure {
				context,
				underlying: Underlying::TextureCreationError(e),
			})?;

			let eye_depth_renderbuffer = DepthRenderBuffer::new(
				facade, DepthFormat::F32, eye_width, eye_height,
			).map_err(|e| Error::OpenglFailure {
				context,
				underlying: Underlying::RenderBufferCreationError(e),
			})?;

			EyeFrameBuffer::try_new_or_drop(
				Box::new((eye_texture, eye_depth_renderbuffer)),
				|(colour, depth)| {
					SimpleFrameBuffer::with_depth_buffer(facade, &*colour, &*depth)
						.map_err(|e| Error::OpenglFailure { context, underlying: Underlying::ValidationError(e) })
				})
		}

		Ok(StereoscopicFrameBuffer {
			left_eye: new_eye("left eye", facade, eye_width, eye_height)?,
			right_eye: new_eye("right eye", facade, eye_width, eye_height)?,
		})
	}

	/// Uses transmute (rather `unsafe`) under the bonnet!
	pub unsafe fn glium_surfaces<'a>(&'a mut self) -> (&'a mut SimpleFrameBuffer<'a>, &'a mut SimpleFrameBuffer<'a>) {
		// TODO I don't really want nasal demons, can I sort this out?
		let left: &'a mut SimpleFrameBuffer<'a> = std::mem::transmute(self.left_eye.all_mut_erased().frame_buffer);
		let right: &'a mut SimpleFrameBuffer<'a> = std::mem::transmute(self.right_eye.all_mut_erased().frame_buffer);

		(left, right)
	}

	pub fn glium_textures_ref<'a>(&'a self) -> (&'a Texture2d, &'a Texture2d) {
		let left: &'a Texture2d = &self.left_eye.head().0;
		let right: &'a Texture2d = &self.right_eye.head().0;

		(left, right)
	}
}
