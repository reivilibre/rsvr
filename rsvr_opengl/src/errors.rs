use std::error::Error;

use glium::framebuffer::ValidationError;
use glium::texture::TextureCreationError;
use glium::framebuffer::RenderBufferCreationError;
use glium::SwapBuffersError;
use glium::buffer::BufferCreationError;
use glium::index::BufferCreationError as IndexBufferCreationError;
use glium::vertex::BufferCreationError as VertexBufferCreationError;
use std::fmt::{Display, Formatter, Result as FmtResult};
use glium::DrawError;

use rsvr_webrtc::RsvrWebRtcError;

#[derive(Clone, Debug)]
pub enum RsvrOpenglError {
	OpenglFailure {
		context: &'static str,
		underlying: OpenglUnderlyingFailure,
	},
	ContractFailure {
		context: &'static str,
		message: &'static str,
	},
	WebRtcFailure(RsvrWebRtcError),
}

impl From<RsvrWebRtcError> for RsvrOpenglError {
	fn from(e: RsvrWebRtcError) -> Self {
		RsvrOpenglError::WebRtcFailure(e)
	}
}

#[derive(Clone, Debug)]
pub enum OpenglUnderlyingFailure {
	ValidationError(ValidationError),
	TextureCreationError(TextureCreationError),
	RenderBufferCreationError(RenderBufferCreationError),
	SwapBuffersError(SwapBuffersError),
	VertexBufferCreationError(VertexBufferCreationError),
	IndexBufferCreationError(IndexBufferCreationError),
	DrawError(DrawError),
}

impl Display for RsvrOpenglError {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		match self {
			RsvrOpenglError::OpenglFailure { context, underlying } => {
				use self::OpenglUnderlyingFailure::*;
				match underlying {
					ValidationError(e) => {
						write!(f, "{} [{}] caused by {}", self.description(), context, e)
					}
					TextureCreationError(e) => {
						write!(f, "{} [{}] caused by {}", self.description(), context, e)
					}
					RenderBufferCreationError(e) => {
						write!(f, "{} [{}] caused by {}", self.description(), context, e)
					}
					SwapBuffersError(e) => {
						write!(f, "{} [{}] caused by {}", self.description(), context, e)
					}
					VertexBufferCreationError(e) => {
						write!(f, "{} [{}] caused by {}", self.description(), context, e)
					}
					IndexBufferCreationError(e) => {
						write!(f, "{} [{}] caused by {}", self.description(), context, e)
					}
					DrawError(e) => {
						write!(f, "{} [{}] caused by {}", self.description(), context, e)
					}
				}
			}
			RsvrOpenglError::ContractFailure { context, message } => {
				write!(f, "{}; context: {}", message, context)
			}
			RsvrOpenglError::WebRtcFailure(underlying) => {
				write!(f, "WebRTC Failure: {:?}", underlying)
			}
		}
	}
}

impl Error for RsvrOpenglError {}