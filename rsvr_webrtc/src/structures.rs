use gstreamer::{BufferPool, Caps, BufferPoolExt, BufferPoolConfig, BufferPoolExtManual, ElementFactory};
use gstreamer_app::AppSrc;
use crate::implementation::AppControl;
use glib::object::Cast;

#[derive(Debug, Clone)]
pub enum RsvrWebRtcError {
	InitialisationProblem(String),
	UnexpectedProblem(String),
}

pub struct WebRtcState {
	pub buffer_pool: BufferPool,
	pub video_application_source: AppSrc,
}

impl WebRtcState {
	/// Creates a new WebRTC State struct
	/// Pass `true` to init_gstreamer the first time to create one,
	/// **unless** GStreamer is already used elsewhere in your application.
	pub fn new(init_gstreamer: bool, width: u32, height: u32, framerate: (i32, i32),
	           server: String, connection_id: String) -> Result<Self, RsvrWebRtcError> {
		if init_gstreamer {
			gstreamer::init()
				.map_err(|e| RsvrWebRtcError::InitialisationProblem(
					format!("Unable to initialise GStreamer: {:?}", e)
				))?;
		}

		let buffer_pool = BufferPool::new();

		let framerate_fraction = gstreamer::Fraction::new(framerate.0, framerate.1);

		/*let video_caps = Caps::builder("video/x-raw")
			.field("format", &"RGB")
			.field("width", &width)
			.field("height", &height)
			.field("framerate", &framerate_fraction)
			.build();

		eprintln!("vc {:#?}", video_caps);*/


		use gstreamer_video::{VideoInfo, VideoFormat};
		use gstreamer::Fraction;
		let video_info =
			VideoInfo::new(VideoFormat::Rgb, width, height)
				.fps(framerate_fraction)
				.build()
				.expect("Failed to create video info.");

		eprintln!("vi {:#?}", video_info);
		eprintln!("vi.to_caps {:#?}", video_info.to_caps().unwrap());

		let video_caps = video_info.to_caps().unwrap();


		let video_appsrc = ElementFactory::make("appsrc", None)
			.expect("Failed to create appsrc element");

		let video_application_source = video_appsrc.clone().dynamic_cast::<AppSrc>()
			.expect("appsrc should be castable to AppSrc!");

		video_application_source.set_caps(&video_caps);

		use std::thread;
		use std::sync::{Condvar, Mutex, Arc};
		let video_appsrc_clone = video_appsrc.clone();
		let negotiation = Arc::new((Condvar::new(), Mutex::new(false)));
		let negotiation_clone = negotiation.clone();
		thread::spawn(move || {
			crate::implementation::start(server, connection_id, &video_appsrc_clone, negotiation_clone);
		});




		const NUM_BYTES_PER_PIXEL: u32 = 3;

		let mut buffer_pool_config = buffer_pool.get_config();
		buffer_pool_config.set_params(&video_caps, width * height * NUM_BYTES_PER_PIXEL, 0, 0);
		buffer_pool.set_config(buffer_pool_config);

		buffer_pool.set_active(true)
			.map_err(|e| RsvrWebRtcError::InitialisationProblem(
				format!("Unable to make the buffer pool active: {:?}", e)
			))?;

		// Now block until we get a negotiation, otherwise we will fail with an error
		loop {
			let guard = negotiation.1.lock().unwrap();
			let guard = negotiation.0.wait(guard).unwrap();
			if *guard {
				break;
			}
		}

		::std::thread::sleep_ms(5000);


		Ok(WebRtcState {
			buffer_pool,
			video_application_source,
		})
	}

	pub fn submit<F: FnOnce(&mut [u8])>(&mut self, callback: F) -> Result<(), RsvrWebRtcError> {
		let mut buffer = self.buffer_pool.acquire_buffer(None)
			.map_err(|e| RsvrWebRtcError::UnexpectedProblem(
				format!("Failed to acquire buffer: {:?}", e)
			))?;

		{
			let buffer = buffer.get_mut()
				.expect("Impossible: A freshly-created buffer should be mutable");

			let mut buffer_map = buffer.map_writable()
				.ok_or_else(|| RsvrWebRtcError::UnexpectedProblem(String::from("Failed to map buffer writeable")))?;

			callback(buffer_map.as_mut_slice());
		}

		let _ = self.video_application_source.push_buffer(buffer);

		Ok(())
	}
}

impl Drop for WebRtcState {
	fn drop(&mut self) {
		// End the stream upon Drop
		let _ = self.video_application_source.end_of_stream();
	}
}

