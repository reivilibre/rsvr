#![feature(custom_attribute)]

#[macro_use]
extern crate serde_derive; // TODO for some reason, required when publishing — why?

mod implementation;
mod structures;

pub use self::structures::{WebRtcState, RsvrWebRtcError};