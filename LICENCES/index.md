# Licences within the RSVR Project

## All crates

All `rsvr` crates are under the [MIT (Expat) Licence](rsvr_2018_MIT_Expat.txt).

## `rsvr_webrtc` Crate

This crate is based on code from the [gst-webrtc-demos repository](https://github.com/centricular/gstwebrtc-demos), under the following licence:

* [BSD 2-clause licence; Centricular; 2017](Centricular_2017_BSD2.txt)

