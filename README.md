# RSVR: VR in Rust

This workspace of crates is intended to implement an experimental Virtual Reality system in Rust, using inexpensive hardware (or hardware that is already likely to be owned). The current plan is to support the combination of a mobile phone (or similar) and appropriate head-mount.

Please see [the licences](LICENCES/index.md) if you intend to use, redistribute or modify this software. The licences of the dependencies may also need to be consulted.

## Current Progress

Currently, there is basic support for Stereoscopic Rendering implemented in OpenGL using the Glium crate.
There is a lot of work still to be done.

### `rsvr_profile`

The `rsvr_profile` crate specifies the data contained within `.rsvr` files; these will be used to set head-mount or phone parameters. They are also currently used to choose to render locally or over WebRTC.

The structure still hasn't been fully ironed out to be neat, though, so changes are expected.

### `rsvr_opengl`

The `rsvr_opengl` crate can be used alongside an OpenGL application in order to perform 


### `rsvr_webrtc`

The `rsvr_webrtc` crate is used to stream video (and eventually audio) output to e.g. a mobile phone, using WebRTC. Performance of `rsvr_opengl` and `rsvr_webrtc` used together is currently very lacking!

Use it with the code in the `rsvr_webrtc_jsclient` directory and the signalling server for the GStreamer WebRTC demos (note: `--disable-ssl` is required on the signalling server).

### `rsvr_tweak`

The `rsvr_tweak` crate implements an OBJ model renderer, making use of `rsvr_opengl` to draw stereoscopically. It is also capable of tweaking your head-mount/display profile on the fly (though exporting these values remains to be implemented).
